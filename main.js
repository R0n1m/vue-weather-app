new Vue({
    el: ".app",
    data() {
        return {
            key: "e6aea0474f2b4a44e1f0c3ddadc0b4ed",
            base: "https://api.openweathermap.org/data/2.5/",
            query: "",
            weather: {}
        };
    },
    methods: {
        getWeather(e) {
            if (e.key == "Enter") {
                fetch(
                        `${this.base}weather?q=${this.query}&units=metric&APPID=${this.key}`
                    )
                    .then(res => {
                        return res.json();
                    })
                    .then(this.setResults);
            }
        },
        setResults(results) {
            this.weather = results;
            this.query = "";
        },
        dateBuilder() {
            let d = new Date();
            let months = [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ];
            let days = [
                "Sunday",
                "Monday",
                "Tuesday",
                "Wednesday",
                "Thursday",
                "Friday",
                "Saturday"

            ];
            let day = days[d.getDay()];
            let date = d.getDate();
            let month = months[d.getMonth()];
            let year = d.getFullYear();

            return `${day} ${date} ${month} ${year}`;
        }
    }
});